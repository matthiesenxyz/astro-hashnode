# @matthiesenxyz/astro-hashnode

## 0.1.12

### Patch Changes

- 0d3d0ff: fix tailwind

## 0.1.11

### Patch Changes

- 87777a6: [internal] Upgrade AIK from 0.7.0 to 0.8.0

## 0.1.10

### Patch Changes

- 85e272e: Bump dependencies:

  - @tailwindcss/vite from to
  - tailwindcss from to
  - @astrojs/node from to
  - astro from to

## 0.1.9

### Patch Changes

- 56ce8e3: set AIK to only be able to auto-upgrade to latest minior

## 0.1.8

### Patch Changes

- 6ccd6dc: Bump dependencies:

  - @tailwindcss/vite from to
  - astro-font from to
  - tailwindcss from to
  - @astrojs/node from to
  - astro from to

## 0.1.7

### Patch Changes

- 6464de2: [fix] add extra fallback option for ogImage

## 0.1.6

### Patch Changes

- cf327f2: Update README.md
- e54bfbb: [Internal] better handling of the `hashnodeURL` input to verify that including `http` or `https` in the URL does not break the entire integration

## 0.1.5

### Patch Changes

- 784cb63: view transition updates

## 0.1.4

### Patch Changes

- 337a5b0: added new keyword "cms" and updated Dependencies

## 0.1.3

### Patch Changes

- fc94343: This updates internal `AIK` as well as impliments ViewTransitions with a disable switch in the user config

## 0.1.2

### Patch Changes

- 7733762: Add SSR support for blog posts (Tags are still prerendered)

## 0.1.1

### Patch Changes

- d86777f: adds exports for HashnodeAPI, as well as components so users can import the pre-built components for their own layout

## 0.1.0

### Minor Changes

- 3a32776: Initial Release!
