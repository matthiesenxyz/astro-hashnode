import type { Options } from "./schemas/user-config";
import type { AstroIntegration } from "astro";

/**
 * Astro-Hashnode Integration - A Hashnode Integration for Astro
 * 
 * By: MatthiesenXYZ
 * 
 * @see https://github.com/matthiesenxyz/astro-hashnode 
 */
export default function astroHashnode(opts: Options): AstroIntegration